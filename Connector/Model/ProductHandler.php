<?php
namespace Itdotmedia\Connector\Model;

class ProductHandler
	extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
	implements \Itdotmedia\Connector\Api\ProductHandlerInterface
{
	
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager = null;
	
	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $_scopeConfig = null;

	/**
	 * @var Magento\Eav\Model\Config
	 */
	protected $_eavConfig= null;
	
	 
	/**
	 * 
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \Magento\Eav\Model\Config $eavConfig
	 */
	public function __construct( 
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Customer\Model\ResourceModel\Group\Collection $groupCollection,
		\Magento\Eav\Model\Config $eavConfig
	) {
		$this->_storeManager = $storeManager;
		$this->_scopeConfig = $scopeConfig;
		$this->_groupCollection = $groupCollection;
		$this->_eavConfig = $eavConfig;
	}
	
	/**
	* {@inheritdoc}
	*/
	public function getAllOptions() {
		// nothing
	}

	/**
	* {@inheritdoc}
	*/
	public function getAllAttributes() {

		if (!$this->_options) {
			$storeId = $this->_storeManager->getStore()->getId();
			
			/**
			* @var Magento\Eav\Model\Entity\Attribute\AbstractAttribute
			*/
			$attributes = $this->_eavConfig->getEntityAttributes('catalog_product');
			$optionsEav = [];
			foreach($attributes as $attribute) {
				$option = [
					'id' => $attribute->getAttributeId(),
					'name' => $attribute->getName(),
					'options' => [],
				];

				foreach($attribute->getOptions() as $attributeOption) {
					if(!empty($attributeOption['value'])) {
						$option['options'][] = [
							'id'  	=> $attributeOption['value'],
							'label'	=> $attributeOption['label']
						];
					}
				}
				$optionsEav[$attribute->getAttributeId()] = $option;
			}
			$this->_options[] = [
				'type'	=> 'EAV',
				'items' => $optionsEav,
			];
			
			
			
			
		}
		return $this->_options;
	}

}