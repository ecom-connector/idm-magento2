<?php
namespace Itdotmedia\Connector\Model;
use Magento\Framework\App\ResourceConnection;

class OrderStatusHandler
	extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
	implements \Itdotmedia\Connector\Api\OrderStatusHandlerInterface
{
	
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager = null;
	
	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $_scopeConfig = null;

	/**
	 * @var \Magento\Sales\Model\ResourceModel\Order\Status\Collection
	 */
	protected $_orderStatusCollection = null;
	
	/** 
	* @var \Magento\Framework\App\ResourceConnection 
	*/
	private $resourceConnection;
    	
	/**
	 * 
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \Magento\Sales\Model\ResourceModel\Order\Status\Collection $orderStatusCollection
	 */
	public function __construct( 
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Sales\Model\ResourceModel\Order\Status\Collection $orderStatusCollection,
		ResourceConnection $resourceConnection
	) {
		$this->_storeManager = $storeManager;
		$this->_scopeConfig = $scopeConfig;
		$this->_orderStatusCollection = $orderStatusCollection;
		$this->resourceConnection = $resourceConnection;
	}
	
	/**
	* {@inheritdoc}
	*/
  public function getAllOptions($withEmpty = true) {
		$options = [];
		
		// get all order status items
		$orderStatusCodes= $this->_orderStatusCollection->toOptionArray();
		foreach($orderStatusCodes as $orderStatusCode) {
			$options[$orderStatusCode['value']	] = [
				'id'  	=> $orderStatusCode['value'],
				'code'	=> $orderStatusCode['value'],
				'label'	=> $orderStatusCode['label']
			];
		}
		
		// fill with paired state
		$TABLE_SALES_ORDER_STATUS_STATE = $this->resourceConnection->getTableName('sales_order_status_state');
		$sql = "
			SELECT	* 
			FROM		".$TABLE_SALES_ORDER_STATUS_STATE."
			WHERE		status in ('" . implode("', '", array_keys($options)) . "')";
		
		$connection = $this->resourceConnection->getConnection();
		$query = $connection->query($sql);
		while ($rec = $query->fetch()) {
			$options[$rec['status']]['state'] = $rec['state'];
		}
		
		return $options;
	}

}